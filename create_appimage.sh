#!/bin/bash

. version

# https://wps-linux-personal.wpscdn.cn/wps/download/ep/Linux2019/11664/wps-office-11.1.0.11664-1.x86_64.rpm
# https://wps-linux-personal.wpscdn.cn/wps/download/ep/Linux2019/11664/wps-office_11.1.0.11664_amd64.deb
# WPS_FILE="wps-office-${WPS_VERSION}.${WPS_RELEASE}-1.x86_64.rpm"
WPS_FILE="wps-office_${WPS_VERSION}.${WPS_RELEASE}_amd64.deb"
WPS_URL="https://wps-linux-personal.wpscdn.cn/wps/download/ep/Linux2019/${WPS_RELEASE}/${WPS_FILE}"

APPDIR="WPS_Office_AppDir"
mkdir -p ${APPDIR}
wget -c ${WPS_URL} -O ${APPDIR}/${WPS_FILE}
cp -r AppRun ${APPDIR}
cp -r com.wps.Office.desktop ${APPDIR}
cp -r wps-launcher ${APPDIR}

pushd ${APPDIR}
chmod +x AppRun
# unrpm ${WPS_FILE}
dpkg -X ${WPS_FILE} .
rm -rf ${WPS_FILE}
cp -r usr/share/icons/hicolor/256x256/mimetypes/wps-office2019-kprometheus.png .
cp -r usr/share/icons/hicolor/256x256/mimetypes/wps-office2019-kprometheus.png ./.DirIcon
popd

cp -r /usr/lib/x86_64-linux-gnu/libstdc++.so.6 ${APPDIR}/opt/kingsoft/wps-office/office6/libstdc++.so.6


# wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
chmod +x appimagetool-x86_64.AppImage

./appimagetool-x86_64.AppImage ${APPDIR}

rm -rf ${APPDIR}
